class SubscribersController < ApplicationController
  def create 
    @subscriber = Subscriber.new(subscriber_params)
    if @subscriber.save 
      cookies[:saved_subscriber] = true
      redirect_to root_path, notice: "EMAIL SUBSCRIBED!"
      cookies.delete :saved_subscriber
    else 
      redirect_to root_path, notice: "SUBMISSION ERROR"
    end 
  end
  
  private   
    def subscriber_params
      params.require(:subscriber).permit(:email)
    end
end
