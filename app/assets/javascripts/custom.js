function menuToggle(){
  var menu = document.getElementById("menu");
  menu.addEventListener("click", function changeClass(){
    if ( document.getElementById("menu").classList.contains('fa-bars') ){
      document.getElementById("menu").classList.remove('fa-bars');
      document.getElementById("menu").classList.add('fa-times');
      document.getElementById("contact-li").setAttribute("style", "display: inline ");
      document.getElementById("about-li").setAttribute("style", "display: inline ");
    }
    else {
      document.getElementById("menu").classList.remove('fa-times');
      document.getElementById("menu").classList.add('fa-bars');
      document.getElementById("contact-li").setAttribute("style", "display: none ");
      document.getElementById("about-li").setAttribute("style", "display: none ");
    }

  })
}

function addCollapse(){
  var elementsArray = document.querySelectorAll('coin-element');
  elementsArray.forEach(function(elem) {
      elem.addEventListener("click", function() {
        collapseExpand(elem);
      });
  });
}


var collapsed = true;

function collapseExpand(element){
  if (collapsed){
    element.style.height = "100px";
    element.style.backgroundColor = "white";
    collapsed = false;
  }
  else {
    element.style.height = "50px";
    element.style.backgroundColor = "#414141";
    collapsed = true;
  }
  console.log("Clicked")
}




window.addEventListener("load", menuToggle);
window.addEventListener("load", addCollapse);
