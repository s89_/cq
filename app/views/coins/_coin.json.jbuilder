json.extract! coin, :id, :name, :ranking, :speed, :team, :community, :development, :adoption, :reputation, :wallet, :github, :wiki, :subreddit, :created_at, :updated_at
json.url coin_url(coin, format: :json)
