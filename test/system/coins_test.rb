require "application_system_test_case"

class CoinsTest < ApplicationSystemTestCase
  setup do
    @coin = coins(:one)
  end

  test "visiting the index" do
    visit coins_url
    assert_selector "h1", text: "Coins"
  end

  test "creating a Coin" do
    visit coins_url
    click_on "New Coin"

    fill_in "Adoption", with: @coin.adoption
    fill_in "Community", with: @coin.community
    fill_in "Development", with: @coin.development
    fill_in "Github", with: @coin.github
    fill_in "Name", with: @coin.name
    fill_in "Ranking", with: @coin.ranking
    fill_in "Reputation", with: @coin.reputation
    fill_in "Speed", with: @coin.speed
    fill_in "Subreddit", with: @coin.subreddit
    fill_in "Team", with: @coin.team
    fill_in "Wallet", with: @coin.wallet
    fill_in "Wiki", with: @coin.wiki
    click_on "Create Coin"

    assert_text "Coin was successfully created"
    click_on "Back"
  end

  test "updating a Coin" do
    visit coins_url
    click_on "Edit", match: :first

    fill_in "Adoption", with: @coin.adoption
    fill_in "Community", with: @coin.community
    fill_in "Development", with: @coin.development
    fill_in "Github", with: @coin.github
    fill_in "Name", with: @coin.name
    fill_in "Ranking", with: @coin.ranking
    fill_in "Reputation", with: @coin.reputation
    fill_in "Speed", with: @coin.speed
    fill_in "Subreddit", with: @coin.subreddit
    fill_in "Team", with: @coin.team
    fill_in "Wallet", with: @coin.wallet
    fill_in "Wiki", with: @coin.wiki
    click_on "Update Coin"

    assert_text "Coin was successfully updated"
    click_on "Back"
  end

  test "destroying a Coin" do
    visit coins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Coin was successfully destroyed"
  end
end
