Rails.application.routes.draw do
  resources :coins
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :subscribers
  root 'static_pages#index'
  
  post '/', to:  'subscribers#create'
end
