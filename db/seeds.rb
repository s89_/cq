# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Seeds subscribers 

  Subscriber.create(email: 'sm@smrry.com')
  Subscriber.create(email: 'elon@spacex.com')
  Subscriber.create(email: 'jordan@japan.com')
  Subscriber.create(email: 'jeffb@amazon.com')
  Subscriber.create(email: 'pthiel@paypal.com')




  Coin.create(name: 'Stellar', ranking: 10.0)
  Coin.create(name: 'Bitcoin', ranking: 10.0)
  Coin.create(name: 'Ethereum', ranking: 10.0)
  Coin.create(name: 'XRP', ranking: 10.0)
  Coin.create(name: 'Bitcoin Cash', ranking: 10.0)
  Coin.create(name: 'EOS', ranking: 10.0)
  Coin.create(name: 'Litecoin', ranking: 10.0)
  Coin.create(name: 'Tether', ranking: 10.0)
  Coin.create(name: 'Cardano', ranking: 10.0)
  Coin.create(name: 'Monero', ranking: 10.0)
  Coin.create(name: 'TRON', ranking: 10.0)
  Coin.create(name: 'IOTA', ranking: 10.0)
