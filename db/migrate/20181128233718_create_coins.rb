class CreateCoins < ActiveRecord::Migration[5.2]
  def change
    create_table :coins do |t|
      t.string :name
      t.float :ranking
      t.float :speed
      t.float :team
      t.float :community
      t.float :development
      t.float :adoption
      t.float :reputation
      t.float :wallet
      t.string :github
      t.string :wiki
      t.string :subreddit

      t.timestamps
    end
  end
end
